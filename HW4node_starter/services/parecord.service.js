const db = require('../_helpers/database');
const PArecord = db.PArecord;
const User = db.User;


module.exports = {
      getAllPArecords,
      addPArecord,
      deletePArecord
}


async function getAllPArecords() {
    return await PArecord.find().populate('createdBy');
}

async function deletePArecord(date, username) {
    console.log('date = ', date);
    console.log('username = ', username);
    PArecord.findOneAndRemove({createdDate: date, createdBy: username})
        .then(response => {
            // console.log(response)
        })
        .catch(err => {
            // console.error(err)
        });
}


async function addPArecord(parecord, username) {

    // validate
    console.log(username);
    if (await PArecord.findOne({ createdBy: username, createdDate: parecord.createdDate  })) {
        throw 'Parecord created by"' + parecord.createdBy +" on "+ parecord.createdDate +'" already exists';
    }
    else if(!username){
        throw 'Error with the user submitting the request. User information missing. Malformed request.';
    }
    //populate missing fields in the parecord object
    let newrecord= parecord;
    parecord.createdBy = username;
    parecord.createdDate =  Date.now();

    dbrecord = new PArecord(newrecord);


    // save the record
    await dbrecord.save();

}
